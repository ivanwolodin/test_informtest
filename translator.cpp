#include "translator.h"
#include "qdebug.h"
#include <QRegularExpression>

Translator::Translator(QWidget *parent) : QWidget(parent)
{

}

int Translator::dataValidation(QString &raw_data)
{
    /*Check data consistency
    *
    * return:
    * -1 ---> error, processing is impossible
    *  1 ---> it is Morse code, must be then translated to Latin
    *  2 ---> It is Latin, must be translated to Morse code
    */
    int average_symbol_character_number = 3; // since around 3 chars is one latin symbol
    if (raw_data == ""){
        return -1;
    }
    raw_data = raw_data.toUpper();
    QRegularExpression MorsePattern("[.-]{1,5}(?> [.-]{1,5})*(?>   [.-]{1,5}(?> [.-]{1,5})*)*");
//    QRegularExpressionMatch match = MorsePattern.match(raw_data);
//    bool hasMatchMorse = match.hasMatch();

    QRegularExpression LatinPattern("[A-Z0-9]");
//    QRegularExpressionMatch match2 = LatinPattern.match(raw_data);
//    bool hasMatchLatin = match2.hasMatch();

     if (raw_data.count(LatinPattern) > raw_data.count(MorsePattern)/average_symbol_character_number){
         return 2;
     }
     else if (raw_data.count(LatinPattern) < raw_data.count(MorsePattern)) {
         return 1;
     }
     else if (raw_data.count(LatinPattern) == raw_data.count(MorsePattern)/average_symbol_character_number
              and raw_data.count(LatinPattern) != 0) {
         return 1; // Let Morse code be preferrable
     }
     else if (raw_data.count(LatinPattern) == 0 and  raw_data.count(MorsePattern) == 0){
         return -1;
     }
     else {
         return -1;
     }
}


QString Translator::morseToLatin(QString &raw_data)
{
    /**/
    QMap<QString, QString> morse_to_latin = {
        {"\n"   , "\n",},
        {".-"   , "A", },
        {"-..." , "B", },
        {"-.-." , "C", },
        {"-.."  , "D", },
        {"."    , "E", },
        {"..-." , "F", },
        {"--."  , "G", },
        {"...." , "H", },
        {".."   , "I", },
        {".---" , "J", },
        {"-.-"  , "K", },
        {".-.." , "L", },
        {"--"   , "M", },
        {"-."   , "N", },
        {"---"  , "O", },
        {".--." , "P", },
        {"--.-" , "Q", },
        {".-."  , "R", },
        {"..."  , "S", },
        {"-"    , "T", },
        {"..-"  , "U", },
        {"...-" , "V", },
        {".--"  , "W", },
        {"-..-" , "X", },
        {"-.--" , "Y", },
        {"--.." , "Z", },
        {".----", "1", },
        {"..---", "2", },
        {"...--", "3", },
        {"....-", "4", },
        {".....", "5", },
        {"-....", "6", },
        {"--...", "7", },
        {"---..", "8", },
        {"----.", "9", },
        {"-----", "0", },
        {".-.-.-", ".", },
        {"--..--", ",", },
        {"..--..", "?", },
        {" ",   " "}, // space between symbols within a sing word
        {"/",   " "}  // space is a slash and vice versa
};
    QString response;
    QString key;
    QStringList temp;
    QStringList myStringList = raw_data.split('/'); // got all string devided by '/'

    for (int i=0; i<myStringList.size(); i++){
        temp = myStringList[i].split(" ");
        qDebug()<<temp;
        for (int j=0; j<temp.size(); j++){
            key = temp[j];
            qDebug()<<key;
            response += (morse_to_latin[key]);
            qDebug()<<response;
        }
        response += " ";
    }
    return response;
}

QString Translator::latinToMorse(QString &raw_data)
{
    /**/
    QMap<QString, QString> latin_to_morse = {
        {"\n", "\n"},
        {"A", ".-"},
        {"B", "-..."},
        {"C", "-.-."},
        {"D", "-.."},
        {"E", "."},
        {"F", "..-."},
        {"G", "--."},
        {"H", "...."},
        {"I", ".."},
        {"J", ".---"},
        {"K", "-.-"},
        {"L", ".-.."},
        {"M", "--"},
        {"N", "-."},
        {"O", "---"},
        {"P", ".--."},
        {"Q", "--.-"},
        {"R", ".-."},
        {"S", "..."},
        {"T", "-"},
        {"U", "..-"},
        {"V", "...-"},
        {"W", ".--"},
        {"X", "-..-"},
        {"Y", "-.--"},
        {"Z", "--.."},
        {"1", ".----"},
        {"2", "..---"},
        {"3", "...--"},
        {"4", "....-"},
        {"5", "....."},
        {"6", "-...."},
        {"7", "--..."},
        {"8", "---.."},
        {"9", "----."},
        {"0", "-----"},
        {".", ".-.-.-"},
        {",", "--..--"},
        {"?", "..--.."},
        {" ",   "/"}
};


    QString response;
    QString key;
    for (int i=0; i<=raw_data.size(); i++){
        key = raw_data[i];
        response += (latin_to_morse[key]);
        response += " ";
    }

    return response;
}


