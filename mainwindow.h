#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <translator.h>
#include <QMouseEvent>
#include <QObject>
#include <QEvent>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Translator *converter_obj;
private:
    Ui::MainWindow *ui;
    void keyReleaseEvent(QKeyEvent *event); // popup closes when clicked on
//    bool eventFilter();

public slots:
    void outputProcessedText();
    void aboutTheProgram();
    void writeInfoToFile();
    void readInfoFromFile();
};
#endif // MAINWINDOW_H
