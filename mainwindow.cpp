#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qdebug.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("Morse-Latin Translator");
    QCoreApplication::instance()->installEventFilter(this);
    converter_obj = new Translator();       // instantiation of processing object
    connect(ui->processed_button,          // who is calling signal
            SIGNAL(clicked(bool)),         // signal itself
            this,                          // who udergoes
            SLOT(outputProcessedText()));  // what to do, calling slot

    connect(ui->actionAbout_the_program,
            SIGNAL(triggered(bool)),
            SLOT(aboutTheProgram()));

    connect(ui->actionSave_to_file,
            SIGNAL(triggered(bool)),
            SLOT(writeInfoToFile())
            );
    connect(ui->actionRead_from_file,
            SIGNAL(triggered(bool)),
            SLOT(readInfoFromFile()));

    connect(ui->output_button,
            SIGNAL(clicked(bool)),
            SLOT(writeInfoToFile()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Colon or
            event->key() == Qt::Key_Asterisk or
            event->key() == Qt::Key_Plus // etc
            ) {
            ui->raw_text->textCursor().deletePreviousChar();
        }
}
//bool MainWindow::eventFilter()
//{
//    return false;
//    if (object == this && event->type() == QEvent::KeyPress) {
//            QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
//            if (keyEvent->key() == Qt::Key_Tab) {
//                // Special tab handling
//                return true;
//            } else
//                return false;
//        }
//        return false;
//}
void MainWindow::outputProcessedText()
{
    QString raw_string = ui->raw_text->toPlainText();
    int case_to_act = converter_obj->dataValidation(raw_string);
    qDebug()<<case_to_act;
    ui->processed_text->setText(QString::number(case_to_act));
    switch ( case_to_act ) {
       case 1:
         ui->status_info->setText("Morse-->Latin");
         ui->processed_text->setText( converter_obj->morseToLatin(raw_string));
         break;
       case 2:
         ui->status_info->setText("Latin-->Morse");
         ui->processed_text->setText( converter_obj->latinToMorse(raw_string));
         break;
       case -1:
         ui->status_info->setText("Invalid input data");
         break;
    }
}

void MainWindow::aboutTheProgram()
{
    QMessageBox::information(0,"About program","Program: Morze-Latin Translator\n"
                                                       "Version: 2.1\n"
                                                       "Author: Ivan Volodin\n"
                                               "");
}

void MainWindow::writeInfoToFile()
{
    QString fileName = QFileDialog::getSaveFileName(this,
            tr("Save Data"), "",
            tr("Data (*.txt);;All Files (*)"));
    if (fileName.isEmpty())
           return;
       else {
           QFile file(fileName);
           if (!file.open(QFile::WriteOnly | QFile::Text)) {
               QMessageBox::information(this, tr("Unable to open file"),
                   file.errorString());
               return;
           }

       QString InitialText = ui->raw_text->toPlainText();
       QString ProcessedText = ui->processed_text->toPlainText();
       //qDebug()<<info;

       ProcessedText.replace("\n","\r\n");
       QTextStream out(&file);
               out << "Intial Text:"<<endl;
               out << InitialText<< endl<<endl;
               out << "Processed Text:"<<endl;
               out << ProcessedText.toUtf8();
           }

}

void MainWindow::readInfoFromFile()
{
    QString raw_text;
    QString fileName = QFileDialog::getOpenFileName(this,
            tr("Read Data"), "",
            tr("Data (*.txt);;All Files (*)"));
    if (fileName.isEmpty())
           return;
       else {
           QFile file(fileName);
           if (!file.open(QIODevice::ReadOnly)) {
               QMessageBox::information(this, tr("Unable to open file"),
                   file.errorString());
               return;
           }

    QTextStream in(&file);
           raw_text = in.readAll() ;
    }
    ui->raw_text->setText(raw_text);
    //qDebug()<<raw_text;
}

