#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QWidget>
#include <QMap>
#include <QString>
#include <QVector>
#include <cmath>

class Translator : public QWidget
{
    Q_OBJECT
public:
    explicit Translator(QWidget *parent = nullptr);


signals:

public slots:
    int dataValidation(QString &);
    QString morseToLatin(QString &);
    QString latinToMorse(QString &);
};
#endif // TRANSLATOR_H

